#!/usr/bin/env node

/*
Dieses zusammengehackte Skript generiert Beschreibungen für Lightning Talks. Dazu kannst du einfach den Inhalt des "USER DATA" und evtl. "ESCAPING AND OUTPUT" Abschnitts bearbeiten.
*/

// USER DATA

const description_de = `Mehrere Kurzvorträge verschiedener Personen schnell hintereinander weg. Eininge bereits geplante Lightningtalks sind in den folgenden Eventslots zu sehen. Es wird aber auch spontane geben.`
const description_en = `Several short talks held by different persons quickly one after anohter. Some already planned lightning talks can be seen in the event slots below.`

const lightningTalks1 = [
//{"title": "Full Example", "author": "Jane Doe", "email": "janedoe@example.com", "assets": [{"file": "sit2021.svg", "icon": "file-pdf-o"}]},
{"title": "Carnivore and happy (es geht um Pflanzen)", "author": "Anna-Lena"},
{"title": "What is wrong with TypeScript", "author": "xoria", "email": "sIT2021@xoria.de"},
{"title": "Polyglot - Quelltext in mehreren Sprachen gleichzeitig", "author": "Asterix", "email": "feconi@posteo.de"},
{"author": "Schneider", "email": "vimschneider@webschneider.org", "title": "Moderne Softwareentwicklung mit Großmutters Technik - Wie ich mit vim, einem Terminal und weiteren alten Tools meinen Lebensunterhalt verdiene"},
{"title": "Lightning-Talk-Ception – Hobbyprojekte", "author": "xoria", "email": "sIT2021@xoria.de"},
//{"title": "", "author": "", "email": ""},
]

/*
    {"time": "19:00", "presentation": "dateiformate"},
    {"time": "19:10", "presentation": "tiling"},
    {"time": "19:20", "presentation": "ronils"},
    {"time": "19:30", "presentation": "firefox"},
*/
const lightningTalks2 = [
    {"title": "Dateiformate - Was steht eigentlich in RFCs?", "author": "Asterix", "email": "feconi@posteo.de"},
    {"title": "Tiling Window Manager - Use your desktop efficiently", "author": "xoria", "email": "sIT2021@xoria.de"},
    {"title": "das Gamification-Framework Octalysis", "author": "Lorenz", "email": "me@glor.me"},
    {"title": "Ronils - Die Software hinter studentischen Gremiensitzungen", "author": "Asterix", "email": "feconi@posteo.de"},
    {"title": "Useful firefox addons", "author": "xoria", "email": "sIT2021@xoria.de"},
    //{"title": "Data Mining kann jede*r! – Prüfungskalender der Informatik", "author": "Ana", "email": "anna.boesemann@stud.uni-goettingen.de"},
]

/*
    {"time": "15:00", "presentation": "vim"},
    {"time": "15:15", "presentation": "score"},
    {"time": "15:30", "presentation": "turing"},
    {"time": "15:45", "presentation": "baba"}
*/
const lightningTalks3 = [
    {"author": "xoria", "email": "sIT2021@xoria.de", "title": "Aiming for the highest score"},
    {"author": "Asterix", "email": "feconi@posteo.de", "title": "Turingvollständigkeit in Spielen"},
    {"author": `Asterix <a href=\"mailto: Asterix <a href=\"mailto:feconi@posteo.de\"><i class=\"fa fa-fw fa-envelope-o\"></i></a>, xoria <a href=\"mailto:sIT2021@xoria.de\"><i class=\"fa fa-fw fa-envelope-o\"></i></a>`, "title": "Baba is us"},
]

const allLightningTalks = {
    lightningTalks1, lightningTalks2, lightningTalks3
}

// INTERNAL DATA

function render(lightningTalks, description) {
    const innerList = lightningTalks.map(t => {
        let email, assets;
        if(t.email)
            email = ` <a href="mailto:${t.email}"><i class="fa fa-fw fa-envelope-o"></i></a>`
        if(t.assets)
            assets = t.assets.map(a => ` <a href="/archive/2021/assets/${a.file}" target="_blank"><i class="fa fa-fw fa-${a.icon || "file-pdf-o"}"></i></a>`).join("");
        return `<li>${t.title} (${t.author}${email || ""})${assets || ""}</li>`;
    }).join("");
    return `${description}<br><br><ul>${innerList}</ul>`;
}

// ESCAPING AND OUTPUT

// Object.keys(allLightningTalks).forEach(key => {
//     console.log(`\n== Deutsche Version ${key} ==`)
// console.log('"' + render(allLightningTalks[key], description_de).replaceAll('"', '\\"') + '"');
// console.log(`\n== Englische Version ${key} ==`)
// console.log('"' + render(allLightningTalks[key], description_en).replaceAll('"', '\\"') + '"');
// })

console.log(`  "lightning1": {
      "language": ["de", "en"],
      "title": "Lightningtalks 1",
      "translate": {
          "de": {
              "short": ${'"' + render(lightningTalks1, description_de).replaceAll('"', '\\"') + '"'}
          },
          "en": {
              "short": ${'"' + render(lightningTalks1, description_en).replaceAll('"', '\\"') + '"'}
          }
      },
      "duration": 60,
      "icon": "comment",
      "color": "info"
  },
  "lightning2": {
      "language": ["de", "en"],
      "title": "Lightningtalks 2",
      "translate": {
          "de": {
              "short": ${'"' + render(lightningTalks2, description_de).replaceAll('"', '\\"') + '"'}
          },
          "en": {
              "short": ${'"' + render(lightningTalks2, description_en).replaceAll('"', '\\"') + '"'}
          }
      },
      "duration": 60,
      "icon": "comment",
      "color": "info"
  },
  "lightning3": {
      "language": ["de", "en"],
      "title": "Lightningtalks 3",
      "translate": {
          "de": {
              "short": ${'"' + render(lightningTalks3, description_de).replaceAll('"', '\\"') + '"'}
          },
          "en": {
              "short": ${'"' + render(lightningTalks3, description_en).replaceAll('"', '\\"') + '"'}
          }
      },
      "duration": 60,
      "icon": "comment",
      "color": "info"
  },`);