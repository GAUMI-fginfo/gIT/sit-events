#!/usr/bin/env node
const fs = require("fs");
const yaml = require("js-yaml");
const doc = yaml.load(fs.readFileSync(`${__dirname}/talks.yaml`, 'utf8'));
const presentations = {};
const schedule = {};
const workshops = [];
const expandedSchedule = doc.schedule.map(ev => ({...doc.defaults, ...ev}));
//fs.writeFileSync(`${__dirname}/talks.json`, JSON.stringify(expandedSchedule));
expandedSchedule.forEach((ev, i) => {
    ev.author = `[${ev.room}]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ev.author}`
    if(ev.type === "talk") {
        presentations[`${i}`] = ev;
        if(schedule[ev.date] === undefined)
            schedule[ev.date] = [];
        schedule[ev.date].push({time: ev.time, presentation: `${i}`});
    } else if(ev.type === "workshop") {
        workshops.push({
            language: [ev.language],
            title: ev.title,
            short: ev.short,
            details: ev.details,
            times: [
                {
                    datetime: `${ev.date} ${ev.time}`,
                    duration: ev.duration
                }
            ],
            author: ev.author
        });
    } else 
        throw new Error(`Unknown event type: ${ev.type}`);
});

fs.writeFileSync(`${__dirname}/presentations.json`, JSON.stringify(presentations, null, 4));
fs.writeFileSync(`${__dirname}/schedule.json`, JSON.stringify(schedule, null, 4));
fs.writeFileSync(`${__dirname}/workshops.json`, JSON.stringify({workshops:workshops}, null, 4));
